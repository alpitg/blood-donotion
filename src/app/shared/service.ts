import { Injectable } from '@angular/core';

export class Employee {
    ID: number;
    BloodType: number;
    Prefix: string;
    Name: string;
    Email: string;
    Contact: string;
    StateID: number;
    CityID: number;
    BirthDate: string;
}

export class State {
    ID: number;
    Name: string;
}

export class City {
    ID: number;
    StateID: number;
    Name: string;
}

export class BloodType {
    ID: number;
    Type: string;
}

const employees: Employee[] = [
    {
        'ID': 1,
        'BloodType': 1,
        'Prefix': 'Mr.',
        'Name': 'Heart',
        'Email': 'CEO',
        'Contact': '5021412141',
        'StateID': 1,
        'CityID': 1,
        'BirthDate': '1995/03/11',
    },
    {
        'ID': 2,
        'BloodType': 2,
        'Prefix': 'Mr.',
        'Name': 'Heart',
        'Email': 'CEO',
        'Contact': '854124584',
        'StateID': 2,
        'CityID': 2,
        'BirthDate': '1964/03/16',
    },
    {
        'ID': 3,
        'BloodType': 3,
        'Prefix': 'Mr.',
        'Name': 'Heart',
        'Email': 'CEO',
        'Contact': '7454845245',
        'StateID': 3,
        'CityID': 3,
        'BirthDate': '1935/07/14',
    },
];

const states: State[] = [
    {
        'ID': 1,
        'Name': 'Maharashtra'
    }, {
        'ID': 2,
        'Name': 'Punjab'
    }, {
        'ID': 3,
        'Name': 'Uttrakhand'
    }
];

const cities: City[] = [
    {
        'ID': 1,
        'StateID': 1,
        'Name': 'Mumbai'
    }, {
        'ID': 2,
        'StateID': 1,
        'Name': 'Jalandhar'
    }, {
        'ID': 3,
        'StateID': 1,
        'Name': 'Dehradun'
    }
];

const bloodTypes: BloodType[] = [
    {
        'ID': 1,
        'Type': 'A+'
    },
    {
        'ID': 2,
        'Type': 'A-'
    },
    {
        'ID': 3,
        'Type': 'B+'
    },
    {
        'ID': 4,
        'Type': 'B-'
    },
    {
        'ID': 5,
        'Type': 'AB+'
    },
    {
        'ID': 6,
        'Type': 'AB-'
    },
    {
        'ID': 7,
        'Type': '0+'
    },
    {
        'ID': 8,
        'Type': 'O-'
    },
];

@Injectable()
export class Service {
    getEmployees() {
        return employees;
    }
    getStates() {
        return states;
    }
    getCities() {
        return cities;
    }
    getBloodType() {
        return bloodTypes;
    }
}
