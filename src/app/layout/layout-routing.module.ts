import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const layoutRoutes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [

            { path: '', redirectTo: 'home', pathMatch: 'fullPath' },

            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'about', loadChildren: './about/about.module#AboutModule' },
            { path: 'contact', loadChildren: './contact/contact.module#ContactModule' },
            { path: 'blood-donors', loadChildren: './blood-donors/blood-donors.module#BloodDonorsModule' },
            { path: 'register', loadChildren: './register/register.module#RegisterModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(layoutRoutes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
