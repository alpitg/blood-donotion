import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const homeRoutes: Routes = [
    { path: '', loadChildren: './layout/layout.module#LayoutModule' }
];

@NgModule({
    imports: [RouterModule.forRoot(homeRoutes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
