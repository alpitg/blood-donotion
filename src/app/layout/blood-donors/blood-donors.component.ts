import { Component, OnInit } from '@angular/core';
import { Employee, State, Service, City, BloodType } from '../../shared/service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blood-donors',
  templateUrl: './blood-donors.component.html',
  styleUrls: ['./blood-donors.component.css']
})
export class BloodDonorsComponent implements OnInit {

  dataSource: Employee[];
  states: State[];
  cities: City[];
  bloodTypes: BloodType[];

  events: Array<string> = [];

  constructor(service: Service, private router: Router) {
    this.initializeGrid(service);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
  }

  initializeGrid(service) {
    this.dataSource = service.getEmployees();
    this.states = service.getStates();
    this.cities = service.getCities();
    this.bloodTypes = service.getBloodType();
  }

  onRegisterButtonClick() {
    this.router.navigate(['register']);
  }

  logEvent(eventName) {
    this.events.unshift(eventName);
  }

  clearEvents() {
    this.events = [];
  }

}
