import { BrowserModule } from '@angular/platform-browser';

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { DxButtonModule, DxDataGridModule } from 'devextreme-angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Service } from './shared/service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    DxDataGridModule,
    DxButtonModule,
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
