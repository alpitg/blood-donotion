import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';

const homeRoutes: Routes = [
    { path: '', component: RegisterComponent }
];

@NgModule({
    imports: [RouterModule.forChild(homeRoutes)] ,
    exports: [RouterModule]
})
export class RegisterRoutingModule { }
