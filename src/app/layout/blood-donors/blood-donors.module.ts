import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CommonModule } from '@angular/common';
import { DxDataGridModule, DxButtonModule } from 'devextreme-angular';
import { BloodDonorsComponent } from './blood-donors.component';
import { BloodDonorsRoutingModule } from './blood-donors-routing.module';


@NgModule({
    declarations: [
        BloodDonorsComponent
    ],
    imports: [
        CommonModule,
        BloodDonorsRoutingModule,
        MDBBootstrapModule,
        DxDataGridModule,
        DxButtonModule,
    ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: []
})
export class BloodDonorsModule { }
