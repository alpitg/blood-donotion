import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BloodDonorsComponent } from './blood-donors.component';

const eventsRoutes: Routes = [
    { path: '', component: BloodDonorsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(eventsRoutes)] ,
    exports: [RouterModule]
})
export class BloodDonorsRoutingModule { }
