import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { DxDataGridModule, DxButtonModule } from 'devextreme-angular';
import { Service } from '../../shared/service';


@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        MDBBootstrapModule,
        DxDataGridModule,
        DxButtonModule
    ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [Service]
})
export class HomeModule { }
