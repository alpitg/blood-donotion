import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
  }

  onRegisterButtonClick() {
    this.router.navigate(['register']);
  }
}
